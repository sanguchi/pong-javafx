package pong;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import java.util.ArrayList;
import java.util.HashMap;

import javafx.geometry.VPos;
import javafx.scene.text.TextAlignment;

public class GameLevel {
    protected PlayerBlock player = new PlayerBlock(0, CanvasMainClass.SCREEN_HEIGHT / 2 - PlayerBlock.BLOCK_HEIGHT / 2);
    protected PlayerBlock cpu = new PlayerBlock(CanvasMainClass.SCREEN_WIDTH - PlayerBlock.BLOCK_WIDTH,CanvasMainClass.SCREEN_HEIGHT / 2 - PlayerBlock.BLOCK_HEIGHT / 2 );

    private GameBall ball = new GameBall(CanvasMainClass.SCREEN_WIDTH / 2 - GameBall.BALL_DIAMETER, CanvasMainClass.SCREEN_HEIGHT / 2 - GameBall.BALL_DIAMETER);

    private boolean paused = false;
    //0 = EASY, 1 = MEDIUM, 2 = HARD
    public int difficult = 1;
    public ArrayList<String> keys = new ArrayList<String>();
    public HashMap<String, Integer> keyPresses = new HashMap<String, Integer>();
    private String waitingText = "Press any key to start.";
    private String pausedText = "Press any key.";
    public void update() {
        //System.out.println(this.keys.size());
        if(paused) {
            if(this.keys.size() == 0 ) {
                return;
            }
            else {
                paused = false;
            }
        }

        if(this.keys.contains("UP")) {
            this.player.moveUp();
        }
        if(this.keys.contains("DOWN")) {
            this.player.moveDown();
        }
        /*
        if(keyPresses.containsKey("SPACE") && keyPresses.get("SPACE") == 1) {
            difficult++;
            if(difficult > 2) {
                difficult = 0;
            }
            System.out.println(difficult);
        }
        */
        if(this.keys.contains("DIGIT1")) {
            this.difficult = 0;
        }
        if(this.keys.contains("DIGIT2")) {
            this.difficult = 1;
        }
        if(this.keys.contains("DIGIT3")) {
            this.difficult = 2;
        }
        if(paused) {
            return;
        }
        ball.update(this);
        cpu.udpate(this);
    }

    public void render(GraphicsContext gc) {
        //clear screen
        gc.setFill(Color.BLACK);
        gc.fillRect(0,0, CanvasMainClass.SCREEN_WIDTH, CanvasMainClass.SCREEN_HEIGHT);
        gc.setFill( Color.WHITE );
        //if someone scored or the has not started.
        if(this.paused) {
            gc.setTextBaseline(VPos.BOTTOM);
            gc.fillText( pausedText, CanvasMainClass.SCREEN_WIDTH / 2, CanvasMainClass.SCREEN_HEIGHT );
            gc.strokeText( pausedText, CanvasMainClass.SCREEN_WIDTH / 2, CanvasMainClass.SCREEN_HEIGHT );
        }
        //Convert int score to string.
        String playerScore = Integer.toString(player.score);
        String cpuScore = Integer.toString(cpu.score);
        //Display score.
        gc.setTextBaseline(VPos.TOP);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText(playerScore + " | " + cpuScore, CanvasMainClass.SCREEN_WIDTH / 2, 0);
        gc.strokeText(playerScore + " | " + cpuScore, CanvasMainClass.SCREEN_WIDTH / 2, 0);
        String difficultText = "";
        switch (difficult){
            case 0: difficultText = "EASY"; break;
            case 1: difficultText = "MEDIUM"; break;
            case 2: difficultText = "HARD"; break;
        }
        //Display difficulty.
        gc.setTextAlign(TextAlignment.LEFT);
        gc.fillText(difficultText, 0 , 0);
        gc.strokeText(difficultText, 0, 0);
        //draw player
        gc.fillRect(player.getXpos(), player.getYpos(), PlayerBlock.BLOCK_WIDTH, PlayerBlock.BLOCK_HEIGHT);
        //draw cpu
        gc.fillRect(cpu.getXpos(), cpu.getYpos(), PlayerBlock.BLOCK_WIDTH, PlayerBlock.BLOCK_HEIGHT);
        //draw ball
        gc.fillOval(ball.getXpos(), ball.getYpos(), GameBall.BALL_DIAMETER, GameBall.BALL_DIAMETER);
        //gc.fillOval(10, 60, 10, 10);
        gc.setTextBaseline(VPos.BOTTOM);
        gc.fillText(Double.toString(ball.yvel), 100, CanvasMainClass.SCREEN_HEIGHT);
        gc.fillText(Double.toString(ball.xvel), 200, CanvasMainClass.SCREEN_HEIGHT);
        ball.render(gc);
    }

    public GameBall getBall() {
        return ball;
    }
    public void pause() {
        this.paused = true;
    }
}
